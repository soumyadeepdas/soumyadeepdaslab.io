---
layout: page
title: Blog Posts
description: Blog posts by Soumyadeep Das on Radio Astronomy, Active Galaxies, Imaging and Interferometry, Latex, Python Programming, and research life in general.
identifer: blogpage
page-level: mainpage
permalink: blogs/
---

<ul style="list-style-type:none;">
  {% for post in site.posts %}
    <li>
      <h2 style="font-size: 15pt;"><a aria-label="{{ post.title }} - a blog by Soumyadeep Das on {% for tag in post.tags %}{% capture tag_name %}{{ tag }}{% endcapture %}{{ tag_name }} {% endfor %}." href="{{ post.url | absolute_url }}">{{ post.title }}</a></h2>
      <p><span class="image left">
          
          <picture>
                <source data-srcset="{{ post.image-webp | absolute_url }}" type="image/webp" >
                <source data-srcset="{{ post.image | absolute_url }}" type="image/jpeg" > 
                <img src="{{ post.image-thumb | absolute_url }}" alt="{{ post.image-alt }} - blog {{ post.title }} by Soumyadeep Das" data-src="{{ post.image | absolute_url }}"  class="lazyload" />
                </picture> 
      </span>{{ post.content | strip_html | truncatewords: 60 }}&nbsp;<a href="{{ post.url | absolute_url }}" aria-label="Explore the blog {{ post.title }} on {% for tag in post.tags %}{% capture tag_name %}{{ tag }}{% endcapture %}{{ tag_name }} {% endfor %}by Soumyadeep Das." >(read more)</a></p>    
      <p><i class="fa fa-calendar"></i>&nbsp;&nbsp;<b>Published on :&nbsp;</b>{{ post.date | date: "%b %-d, %Y" }}</p>
    </li>    
      <hr>
  {% endfor %}
</ul>
