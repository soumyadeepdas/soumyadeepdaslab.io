convert $1.$2 -sampling-factor 4:2:0 -strip -quality 85  -interlace Plane -colorspace RGB $1-preview.jpg
convert -resize 375x $1-preview.jpg $1-preview-375.jpg
convert -resize 480x $1-preview.jpg $1-preview-480.jpg
convert -resize 800x $1-preview.jpg $1-preview-800.jpg
convert -resize 90x $1-preview.jpg $1-preview-thumb.jpg
