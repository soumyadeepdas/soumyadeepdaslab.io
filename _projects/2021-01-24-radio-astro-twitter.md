---
title : Radio Astronomy Live - Twitter Portal to the Radio Sky
short-title : Radio Astronomy Live
description: \@AstronomyRadio is a Twitter Astronomy Scicomm bot run by Soumyadeep Das. It retweets posts from Radio Astronomers with a goal to amplify their reach.
Date :  2021-01-24
author: Soumyadeep Das
layout: project-post
image: assets/images/posts/2021-01-24-radio-astro-twitter/banner.jpg
image-alt: \@AstronomyRadio is a Twitter Astronomy Scicomm bot run by Soumyadeep Das. It retweets posts from Radio Astronomers.
permalink: /posts/2021/01/radio-astro-twitter/
page-level: repopage
status: beta
sitemap: false
---
 
<!-- Add images to assets/images/posts/2021-01-24-radio-astro-twitter -->
<!-- Body of your blog post goes here -->
<!-- Set status to 'publ' when you deem the post completed. -->

@AstronomyRadio is a Twitter Astronomy Scicomm bot run by Soumyadeep Das. It retweets posts from Radio Astronomers with a goal to amplify their reach.

## Introduction

## Search Principles

#### Radio Only
#### Radio Research
#### Radio Used Case

## Running this bot

#### Getting Started

#### Running Locally

#### Automating

## Running on Heroku

## Why?
