---
title : Building a comprehensive static website with Jekyll and Github/Gitlab pages.
short-title : Static Web Jekyll
description: How to build a static website with Jekyll and github pages, fully optimized for academics with blogs, code repositories, and dedicated research pages. Advanced tutorials to implement SEO.
Date :  2020-05-29
author: Soumyadeep Das
layout: project-post
image: assets/images/posts/2020-05-29-academic-webpage-jekyll/banner.jpg
image-alt: Jekyll BRB blogs are very helpful in creating static websites.
image-source: Source - scotch.io.
permalink: /posts/2020/05/academic-webpage-jekyll/
page-level: repopage
status: beta 
sitemap: false
---
 
<!-- Add images to assets/images/posts/2020-05-29-academic-webpage-jekyll -->
<!-- Body of your blog post goes here -->
Jekyll is a simple and blog-aware static site generator built in Ruby. In laymen terms, it's just a tool to let you have all the cool features of a full-blown CMS without having to worry about managing a database. This means hosting is extremely easy and scalable since all you're doing is managing a bunch of files.

### External Repositoriess
<ul class="actions" style="margin-bottom: 5px; padding-bottom: 5px;">
    <li style="height: 18; vertical-align: top;"><a href="https://github.com/dassoumyadeep/editorial-website-extended" target="_blank"  rel="noopener noreferrer"  style="font-size: small;" class="tag_marker"> <span>github : editorial-website-extended</span></a></li>
    <li><iframe src="https://ghbtns.com/github-btn.html?user=dassoumyadeep&repo=editorial-website-extended&type=star&count=true" frameborder="0" scrolling="0" width="78" height="20" title="GitHub"></iframe><iframe src="https://ghbtns.com/github-btn.html?user=dassoumyadeep&repo=editorial-website-extended&type=watch&count=true&v=2" frameborder="0" scrolling="0" width="88" height="20" title="GitHub"></iframe><iframe src="https://ghbtns.com/github-btn.html?user=dassoumyadeep&repo=editorial-website-extended&type=fork&count=true" frameborder="0" scrolling="0" width="78" height="20" title="GitHub"></iframe></li>
</ul>
<ul class="actions" style="margin-top: 0; padding-top: 0;">
    <li style="height: 18; vertical-align: top;"><a target="_blank"  rel="noopener noreferrer"  style="cursor: text; color: #111;" > <span>Gitlab Repo (Updated more frequently): </span></a></li>
    <li style="margin-right: 0px; padding-right: 0px;"><a target="_blank"  rel="noopener noreferrer" style="margin-right: 0px; padding-right: 0px;" href="https://gitlab.com/dassoumyadeep/editorial-website-extended" class="tag_btn"><span>gitlab : editorial-website-extended</span></a></li>
</ul>


## Contact/Contribute

Report issues <a target="_blank" href="https://gitlab.com/soumyadeepdas/soumyadeepdas.gitlab.io/issues" rel="noopener noreferrer">here</a>, or send me an <a target="_blank" href="mailto:soumyadeep.das.phy14@iitbhu.ac.in?subject=[GitLab]%20soumyadeepdas.gitlab.io" rel="noopener noreferrer">email</a> to contribute.

<ul class="actions">
<li>Find me on Github: </li>
<li><iframe src="https://ghbtns.com/github-btn.html?user=dassoumyadeep&type=follow&count=true" frameborder="0" scrolling="0" width="200" height="20" title="GitHub"></iframe></li>
</ul>
