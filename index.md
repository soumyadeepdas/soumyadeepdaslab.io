---
layout: home
title: Home
identifier: homepage
page-level: homepage
---



<section>
<!--     <a href="{{ site.design-url }}" aria-label="Go to Soumyadeep Das Web Design and Development Website">
    <div class="myadbox">
    <h2 style="margin-top: 5px; margin-bottom: 5px; color: white;">Looking for a Web Designer?</h2>
    <p style="color: white; font-weight: 500;" >Check out my works here!</p>
</div>
</a> -->
	<header class="major">
		<h2>Whats New</h2>
	</header>
	<div class="posts">
		{% for post in site.data.new %}
<article>
    <p><a  aria-label="Check out {{ post.title }} - {{ post.desc }}, by Soumyadeep Das"  style="color: black; font-weight: 400;" href="{{ post.link  | relative_url }}">{{ post.title }}</a></p>
            <a aria-label="Whats new by Soumyadeep Das - {{ post.title }}. {{ post.desc }} " href="{{ post.link  | relative_url }}" class="image">
                <picture>
                <source data-srcset="{{ post.img-webp | relative_url }}" type="image/webp" >
                <source data-srcset="{{ post.img-jpg | relative_url }}" type="image/jpeg" > 
                <img src="{{ post.img-thumb | relative_url }}" alt="{{post.tite}} - {{ post.desc }} by Soumyadeep Das" data-src="{{ post.img-jpg | relative_url }}"  class="lazyload" />
                </picture> 
                <p style="margin-top: 10px; color: #444444;">{{ post.desc |  strip_html | truncatewords : 30  }}</p>
            </a>
        </article>
  {% endfor %}
		</div>
	</section>

  


<!-- Section -->
<section>
	<header class="major">
		<!-- <a href="{{ 'cv.html' | relative_url }}">Curriculum Vitae</a> -->
		<h2>Skills and Interests</h2>
	</header>
	<div class="features">
		<article>
			<!-- <span class="icon fa-diamond"></span> -->
			<span style="padding-right:15px; display:inline-block;">
                <img alt="Soumyadeep Das is interested in Active Galactic Nuclei Research and Radio Astronomy" src="assets/images/ico-agn-thumb.png" data-src="assets/images/ico-agn.png"   class="lazyload" width="80" />
			</span>
			<div class="content">
				<h3>Active Galactic Nuclei</h3>
				<p>Radio galaxy evolution. AGN restart. AGN-Galaxy relations. Unified theory.</p>
			</div>
		</article>
		<article>
			<!-- <span class="icon fa-paper-plane"></span> -->
			<span style="padding-right:15px; display:inline-block;">
            <img alt="Amateur Astronomy, star gazing, telescope handling, and Astrophotohraphy is the passion of Soumyadeep Das" src="assets/images/ico-telescope-thumb.png" data-src="assets/images/ico-telescope.png"   class="lazyload" width="80" />
			</span>
			<div class="content">
				<h3>Amateur Astronomy</h3>
				<p>Stargazing. Starlores. Messier Hunting. Astrophotohraphy.</p>
			</div>
		</article>
		<article>
			<!-- <span class="icon fa-signal"></span> -->
			<span style="padding-right:15px; display:inline-block;">
                <img alt="Soumyadeep Das plans to pursue his PhD in Radio Astronomy exploring Active Galaxies and Imaging Radio sources" src="assets/images/ico-radio-thumb.png" data-src="assets/images/ico-radio.png"   class="lazyload" width="80" />
			</span>
			<div class="content">
				<h3>Radio Astronomy</h3>
				<p>Radio Interferometry. Calibrations and Imaging. Data Analysis.</p>
			</div>
		</article>
		<article>
			<!-- <span class="icon fa-rocket"></span> -->
			<span style="padding-right:15px; display:inline-block;">
            <img alt="Soumyadeep Das is an avid programmer, proficient in C Python and Web Development and Digital Design" src="assets/images/ico-py-thumb.png" data-src="assets/images/ico-py.png"   class="lazyload" width="80" />
			</span>
			<div class="content">
				<h3>Programming</h3>
				<p>C. Matlab. FORTRAN. Python. Bash. AIPS. CASA. Android.</p>
			</div>
		</article>
		<article>
			<!-- <span class="icon fa-rocket"></span> -->
			<span style="padding-right:15px; display:inline-block;"><img alt="Soumyadeep Das is a full stack developer, digital designer, writer, memer, and gamer" src="assets/images/ico-misc-thumb.png" data-src="assets/images/ico-misc.png"   class="lazyload" width="80" />
			</span>
			<div class="content">
				<h3>Miscellaneous</h3>
				<p>Digital Design. Rise of Nations. DoTA2. Memes.</p>
			</div>
		</article>
	</div>
</section>


<section>
	<header class="major">
		<h2>Affiliation</h2>
	</header>
	<div class="features">

<article style="width: 70%;">
	<span style="padding-right:15px; display:inline-block;">
		<picture>
                <source data-srcset="assets/images/crest_herts-180.webp" type="image/webp" >
                <source data-srcset="assets/images/crest_herts-180.png" type="image/png" > 
                <img src="assets/images/crest-iitbhu-180.png" alt="Soumyadeep Das is doing is PhD at the Centre for Astrophysics Research (CAR), University of Hertfordshire, UK. He is working on galaxy formation and evolution with LOFAR." width="90"  class="lazyload" />
                </picture>
	</span>
	<div class="content">
		<p style="font-size: 90%;"><b>Presently</b><br>
			PhD Student at the Centre for Astrophysics Research (CAR),<br>
		University of Hertfordshire, College Lane Campus<br>
		Hatfield AL10 9AB, United Kingdom.
	</p>
	</div>
</article>
<br>
<article style="width: 70%;">
	<span style="padding-right:15px; display:inline-block;">
		<picture>
                <source data-srcset="assets/images/crest-iitbhu-180.webp" type="image/webp" >
                <source data-srcset="assets/images/crest-iitbhu-180.png" type="image/png" > 
                <img src="assets/images/crest-iitbhu-180.png" alt="Soumyadeep Das graduated with Dual Degree in Engineering Physics from IIT BHU Varanasi, India" width="90"  class="lazyload" />
                </picture>
	</span>
	<div class="content">
		<p style="font-size: 90%;"><b>Graduated.</b><br>
			Dual Degree in Engineering Physics (B.Tech and M.Tech.)<br>
		Indian Institute of Technology (BHU), Varanasi.<br>
		Banaras Hindu University Campus, Uttar Pradesh 221005. India.
	</p>
	</div>
</article>
<br>




<!-- 
<article style="width: auto;">
	<span style="padding-right:15px; display:inline-block;">
	<div class="crest-row" style="width: 200px;" >
  <div class="crest-column">
    <picture>
                <source data-srcset="assets/images/crest-uwa-180.webp" type="image/webp" >
                <source data-srcset="assets/images/crest-uwa-180.png" type="image/png" > 
                <img src="assets/images/crest-uwa-180.png" alt="UWA Perth Crest" width="90"  class="lazyload" />
                </picture>
  </div>
  <div class="crest-column">
    <picture>
                <source data-srcset="assets/images/crest-icrar-180.webp" type="image/webp" >
                <source data-srcset="assets/images/crest-icrar-180.png" type="image/png" > 
                <img src="assets/images/crest-icrar-180.png" alt="ICRAR Crest" width="90"  class="lazyload" />
                </picture>width="100%"/>
  </div>
</div> 
	
</span>
	<div class="content">
		<p style="font-size: 80%;"><b>PhD Student (ICRAR)</b><br>
			Astronomy and Astrophysics.<br>
		    The University of Western Australia<br>
		    35 Stirling Highway, 6009 Perth, Australia.</p>
	</div>
</article>
 -->

</div>


</section>
