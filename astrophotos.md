---
title: Astrophotography
layout: jsimageview
permalink: /astrophotos/
image: assets/images/astrophotos/almostfull-preview-800.jpg
description: Collection of astrophotos of the night sky, milky way, moon by captured and processed by Soumyadeep Das.
identifer: astropage
page-level: mainpage
---

