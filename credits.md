---
layout: page
title: Acknowledgements
identifer: creditpage
page-level: mainpage
permalink: credits/
redirect_from:
  - about/
  - acknowledgements/
sitemap: false
---
<a name="top"></a>
<img style="cursor: default;" src="https://img.shields.io/badge/build-deploy-blue">
<img style="cursor: default;" src="https://img.shields.io/badge/version-{{ site.myversion }}-yellow">
<img style="cursor: default;" src="https://hitcounter.pythonanywhere.com/nocount/tag.svg?url=https%3A%2F%2Fsoumyadeepdas.gitlab.io" alt="Hits">
<img style="cursor:default;" src="https://img.shields.io/badge/license-Creative Commons-red">

Firstly, a huge shoutout to my friends for helping me with this website. Special mentions to Debjyoti Biswas, Anant Jain Gowadia, Kajal Kumari, VSV Surya Prakash, and Ayush Kumar Singh for their technical and creative expertise, and invaluable support.


### Jekyll and Gitlab pages
This site is built by static website generator <a aria-label="Static website in Jekyll created by Soumyadeep Das" href="https://jekyllrb.com/" target="_blank"  rel="noopener noreferrer" >Jekyll</a> and is hosted free of cost on <a aria-label="Static website created by Soumyadeep Das hosted on Gitlab Pages"  href="https://about.gitlab.com/stages-devops-lifecycle/pages/" target="_blank"  rel="noopener noreferrer" >gitlab pages</a>. I have used the latest jekyll version `V4.0`.

This site is built upon the simplistic yet elegant Editorial template hosted on <a aria-label="Static website in Jekyll created by Soumyadeep Das inspired by html5up template"  href="https://html5up.net/editorial" target="_blank"  rel="noopener noreferrer" >html5up.net</a>. Editorial was adapted for Jekyll by <a aria-label="Static website in Jekyll created by Soumyadeep Das based on Andrew Banchich template"  href="https://gitlab.com/andrewbanchich/editorial-jekyll-theme" target="_blank"  rel="noopener noreferrer" >Andrew Banchich</a>.

### Fonts and Icon packs
I have extensively used <a  aria-label="Soumyadeep Das has used fontawesome icons for this personal research website"  href="https://fontawesome.com/v4.7.0/icons/" target="_blank"  rel="noopener noreferrer" >Font Awesome V4.7</a> and <a aria-label="Soumyadeep Das has used academicons for this personal research website"  href="https://jpswalsh.github.io/academicons/" target="_blank"  rel="noopener noreferrer" >Academicons</a> as icon-pack CDN. This website uses the following three webfonts - `Bebas Neue`, `Fira Sans`, `Source Sans Pro`, and `Roboto Mono`, delivered by <a aria-label="Soumyadeep Das has used Google fonts for this personal research website"  href="https://fonts.google.com" target="_blank"  rel="noopener noreferrer" >Google Fonts</a>. Many of the ideas used in this site have been borrowed from <a aria-label="Soumyadeep Das has borrowed ideas from academicpages for this personal research website"  href="https://github.com/academicpages/academicpages.github.io" target="_blank"  rel="noopener noreferrer" >academicpages.github.io</a>. Home page blinds design is inspired by <a aria-label="Soumyadeep Das has borrowed ideas from html5up massively theme for this personal research website"  href="https://html5up.net/massively target="_blank"  rel="noopener noreferrer" >Massively theme</a>.

### Search and Tags
I have used `lunr.js` for searching through posts in this site. <a aria-label="Soumyadeep Das has used lunr js to provide search interface for his personal research website"  href="https://github.com/olivernn/lunr.js" target="_blank"  rel="noopener noreferrer" >Github repo</a> and <a href="https://learn.cloudcannon.com/jekyll/jekyll-search-using-lunr-js/" target="_blank"  rel="noopener noreferrer" >Cloudcannon</a> provide detailed documentation. Tags have been implemented following Long Qian's <a aria-label="Soumyadeep Das has used long qians python library this personal research website"  href="https://longqian.me/2017/02/09/github-jekyll-tag/" target="_blank"  rel="noopener noreferrer" >tutorial</a>. For even lazier folks out there, Arturo Moncada-Torres has <a aria-label="Soumyadeep Das has used Arturo Moncada-Torres python library this personal research website" href="https://arturomoncadatorres.com/automatically-generating-tag-posts-for-github-pages-using-jekyll/" target="_blank"  rel="noopener noreferrer" >modified</a> Long Qian's method to give a more automated, inline approach.

### Image handling and scripts
I have used new, optimized .webp image format with traditional .jpg and .png image formats to provide a better browsing experience. I have implemented lazyloading of images, using aFarkas' <a aria-label="Soumyadeep Das has used aFarkas lazysizes library this personal research website"  href="https://github.com/aFarkas/lazysizes" target="_blank"  rel="noopener noreferrer" >Lazysizes</a> library. Backgrounds, such as those in the landing and 404 pages, are handled by inline, responsive design adapted javascripts. Images can be optimized for web, converted to .webp, and thumbnails generated using .sh scripts provided in `/assets/images/workshop` directory. 

Further, dedicated `sh` scripts in the root directory can be used to generate post files, complete with all necessary liquid tags and placeholders.

### Search Engine Optimization (SEO)
I have used jekyll plugin `jekyll-seo-tag` to streamline SEO. Detailed documentation can be obtained at the <a aria-label="Soumyadeep Das has performed SEO of his personal research website using jekyll-seo-tag"  href="https://github.com/jekyll/jekyll-seo-tag/blob/master/docs/usage.md" target="_blank"  rel="noopener noreferrer" >github repo</a>.

### Sitemap
`jekyll-sitemap` plugin is used to generate [sitemap]({{ 'sitemap.xml' | absolute_url }}) and `robots.txt`, to be used by search engine webmasters. Refer to the <a aria-label="Soumyadeep Das has prepared sitemap of his personal research website using jekyll-sitemap"  href="https://github.com/jekyll/jekyll-sitemap" target="_blank"  rel="noopener noreferrer" >github repo</a> for documentation.

### Compression
To implement page compression on gitlab pages, I have used plugins `jekyll-gzip` and `jekyll-brotli`. Respective repos for <a href="https://github.com/philnash/jekyll-gzip"  aria-label="Soumyadeep Das has performed compression of his personal research website using jekyll-brotli"  target="_blank"  rel="noopener noreferrer" >gzip</a> and <a href="https://github.com/philnash/jekyll-brotli" target="_blank"  rel="noopener noreferrer" >brotli</a> are immensely enlightening.

### 404 Page
The Spacy 404 custom error page was implemented following <a href="https://codepen.io/moso/pen/KEVPJO" target="_blank"  aria-label="404 page of Soumyadeep Das personal research website uses the Spacy 404 custom error page"  rel="noopener noreferrer" >this</a> codepen.io tutorial.

### Miscellaneous
- CSS minification was performed thanks to the free website <a aria-label="Soumyadeep Das has performed CSS minification of his personal research website using cssminifier.com"  href="https://cssminifier.com/" target="_blank"  rel="noopener noreferrer" >cssminifier.com</a>. Critical inline CSS was identified using <a href="https://jonassebastianohlsson.com/criticalpathcssgenerator/" target="_blank"  aria-label="Soumyadeep Das has extracted critical CSS of his personal research website using jonassebastianohlsson criticalpathcssgenerator"  rel="noopener noreferrer" >Critical Path CSS Generator</a>.
- Collapsing lists were implemented using <a href="https://www.w3schools.com/howto/howto_js_collapsible.asp" target="_blank"  aria-label="Soumyadeep Das has used Collapsing lists in his personal research website using W3 Schools tutorial"  rel="noopener noreferrer" >this</a> tutorial.
- I am using an open source hit counter, implemented via <a href="https://hitcounter.pythonanywhere.com/" target="_blank"  rel="noopener noreferrer"  aria-label="Soumyadeep Das has implemented hit counter in his personal research website using pythonanywhere" >hitcounter.pythonanywhere.com</a>. Current hits can be seen the bottom of this page (might not work properly with adblockers and tracking-blockers.)

In addition to the above mentioned, there are several resources that immensely helped me make this website, but have not been explicitly mentioned. I am hugely indebted towards all of them. I will keep updating this page slowly and try to include every single such source.


<hr>
<p  style="font-size: smaller;" ><a href="#top" class="button icon fa-angle-double-up">&nbsp;Back to Top</a></p>
<!-- Image Gallery -->
